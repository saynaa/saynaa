## Loop

Loops are a programming element that repeat a portion of code a set number of times until the desired process is complete.

### While loop
A while loop is a control flow statement that allows code to be executed repeatedly based on a given Boolean condition.
```sa
loop = 1;
while (loop <= 10) {
    print("	do-while-loop: ", loop, "\n");
    
    loop = loop + 1;
}
```
### Do While loop
A do while loop is a control flow statement that executes a block of code at least once, and then repeatedly executes the block, or not, depending on a given boolean condition at the end of the block.

```sa
dowhile = 1;
do {
    print("	do-while-loop: ", dowhile, "\n");
    
    dowhile = dowhile + 1;
} while (dowhile <= 10);
```