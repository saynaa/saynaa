### number

number can be integer or float
```sa
num1 = 10;
num2 = 10.5;
num3[10] = 20;

print("num1 = ", num1);
print("num2 = ", num2);
print("num3[10] = ", num3[10]);
```