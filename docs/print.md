## print

The print() function prints the specified message to the screen, or other standard output device.
```sa
print("Hello World!");
```
it can be several pram
```sa
print("Hello ", "World!");
```
or can enter variable:

```sa
name = "Mohamed";
print("my name: ", name);

year = 2023;
print("this year: ", year);

num = 10.5;
print("num: ", num);
```
you can also use one-pram-print where is the only string you can enter:

```sa
print "Hello World!";
```