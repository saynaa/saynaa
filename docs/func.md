## Function
function is a block of organized code that is used to perform a single task. They provide better modularity for your application and reuse-ability. Depending on the programming language, a function may be called a subroutine, a procedure, a routine, a method, or a subprogram

```sa
function example(){
	print("Hello ", "World!");
}

example();
```

### Function parameters
Functions aren’t very useful if you can’t pass values to them so you can provide a parameter list in the function declaration.

```sa
function fadd(one, two){
	print("	pram one + pram two = ", one + two);
}
fadd(10, 20);
```