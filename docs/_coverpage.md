<p align="center">
<img src="assets/images/logo.png" height="90px" alt="Saynaa Programming Language" title="Saynaa Programming Language">
</p>

# Saynaa <small>1.2.1</small>
> An interpreter programming language.

* Simple and lightweight
* No external dependencies
* Register based virtual machine

[GitHub](https://github.com/mahdiware/saynaa)
[Get Started](README.md)
