## Operators

An operator is a special symbol or phrase that you use to check, change, or combine values. For example, the addition operator (+) adds two numbers, as in **i = 1 + 2**, and the logical AND operator (&&) combines two Boolean values, as in **if (flag1 && flag2)**.
		
### Arithmetic Operators
* Addition (+)
* Subtraction (-)
* Multiplication (*)
* Division (/)
* Remainder (%)

```sa
	n1 = 1 + 2        // equals 3
	n2 = 5 - 3        // equals 2
	n3 = 2 * 3        // equals 6
	n4 = 10 / 2   // equals 5
	n5 = 9 % 4        // equals 1
```

### Assignment Operator
The assignment operator = initialize or update a value:
```sa
	a = 50;       // a = 50
	b = a;        // b = 50
	c = a * b;    // c = 50 * 50
	d[2] = a + b; // d is array, rhs = 50 + 50
```
Please note that contrary to many other programming languages, the assignment operator has no side effect, it means that it does not return any value.

### Comparison Operators
The comparison operators return a Bool value to indicate whether or not the statement is true:

* Equal (==)
* Not equal (!=)
* Less than (<)
* Less than or equal (<=)
* Greater than (>)
* Greater than or equal (>=)
			
```sa
	1 == 1      // true because 1 is equal to 1
	1 != 2      // true because 1 is not equal to 2
	1 < 2       // true because 1 is less than 2
	1 <= 1      // true because 1 is less than or equal to 1
	1 > 2       // false because 1 is not greater than 2
	1 >= 1      // true because 1 is greater than or equal to 1
```

### Logical Operators
The comparison operators return a Bool value to indicate whether or not the statement is true:

* Logical NOT (!)
* Logical AND (&&)
* Logical OR (||)
			
			
```sa
	!1          // false because 1 is true
	1 && 0      // false because one of the two values is false
	1 || 0      // true because one of the two values is true
```
In order to improve code readability the reserved keywords **not, and, or** has been introduces as an alisas to logical operators.