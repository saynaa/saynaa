#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>
#include "include/main.h"
#include "include/builtin.h"
#include "include/utils.h"
#include "include/compiler.h"

#if defined(READLINE)
  #include <readline/readline.h>
  #include <readline/history.h>
  #include <readline/tilde.h>
#endif

static const char *input_file = NULL;
static uint8_t debug_mode, typeAgain = 0;


void sigintHandler(int signum) {
	if(!typeAgain){
		printf("\n\aTo exit, press ^C again or ^D or type exit();\n");
		typeAgain++;
		return;
	}
	// You can perform additional cleanup or actions here if needed
	// ...
	// Exit the program
	exit(0);
}

static void print_help (void) {
	printf("Usage: %s [options] [arguments...]\n", LANGUAGE);
	printf("\n");
	printf("To start the REPL (under maintenance):\n");
	printf("  %s\n", LANGUAGE);
	printf("\n");
	printf("To compile and execute file:\n");
	printf("  %s example.sa\n", LANGUAGE);
	printf("\n");
	printf("Available options are:\n");
	printf("  -v,  --version    show version information and exit\n");
	printf("  -h,  --help       show command line usage and exit\n");
	printf("  -m                print millitime\n");
	
}

static action_type usage(int argc, char **argv) {
	if (argc == 1) return REPL;

	if (argc == 2 && (!strcmp(argv[1], "-v") || !strcmp(argv[1], "--version"))) {
		printf("%s\n", COPYRIGHT);
		exit(0);
	}

	if (argc == 2 && (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))) {
		print_help();
		exit(0);
	}
	
	action_type type = NONE;
	
	for (int i=1; i<argc; i++) {
		if ((!strcmp(argv[i], "-m")) && (i+1 < argc)) {
			input_file = argv[++i];
			type = MSEC;
		}else if((!strcmp(argv[i], "-d")) && (i+1 < argc)){
			input_file = argv[++i];
			debug_mode = true;
		}else{
			input_file = argv[i];
		}
	}

	return type;
}

int main(int argc, char **argv)
{
	// Register the signal handler
	signal(SIGINT, sigintHandler);
	signal(SIGTSTP, sigintHandler);
	size_t size;
	action_type type = usage(argc, argv);
	
	if(type == REPL) {
		#if defined(READLINE)
			repl_mode(argc, argv);
		#else
			printf("readline not supported\n");
			return EXIT_FAILURE;
		#endif
		return 0;
	}
		
	uint8_t *source_code = file_reader(input_file, &size);
	
	if(source_code == NULL)
		return EXIT_FAILURE;
	
	compiler_info info;
	info.code = source_code;
	info.debug = debug_mode;
	info.size = size;
	
	compiler_run(&info);
	
	if(type == MSEC && info.exit_status == Exit_Success){
		printf("\n\nLexer: %.4f ms\n", info.msec[1]);
		printf("Parser: %.4f ms\n", info.msec[2]);
		printf("Visitor: %.4f ms\n", info.msec[3]);
	   	printf("All time: %.4f ms", info.msec[1]+info.msec[2]+info.msec[3]);
	}
	
	munmap((uint8_t *)source_code, size);

	puts("");
	return 0;
}

void repl_mode(int argc, char **argv)
{
	#if defined(READLINE)
		char *input;
		struct token *tokens;
		size_t ntokens;
		int count = 0;
		
		// Enable history
		using_history();
	
		// Set up readline to handle left button
		rl_bind_key('\e', rl_insert); // Enable left button support
		printf("%s\n", COPYRIGHT);
		while ((input = readline(">>> ")) != NULL) {
			if (input[0] == '\0')
				continue;
				
			//add it to the history
			add_history(input);
			//sprintf(input, "%s\n", input);
			for (int i = 0; input[i] != '\0'; i++) {
				count++;
			}
			
			compiler_info info;
			info.code = (uint8_t *)input;
			info.debug = debug_mode;
			info.size = count;
			
			compiler_run(&info);
			
			puts("");
			count = 0;
			free(input);
		}
	#endif
}
