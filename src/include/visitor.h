#pragma once

#ifndef VISITOR_H
#define VISITOR_H
#include <stdbool.h>

static struct vStore {
	size_t size;								 // Current number of variables stored in the varstore
	
	struct {
		const uint8_t *beg;					   // Beginning memory address of the variable name
		ptrdiff_t len;							// Length of the variable name
		size_t array_size;						// Size of the array, if the variable is an array
		uint8_t type;							 // Type of VarStore
		double *numberValue;				// Pointer to the array of variable values
		uint8_t **stringValue;				   // Beginning memory address of the String Value
		bool *boolValue;						// bool variable
		
		const uint8_t *funcbeg;					   // Beginning memory address of the function name
		ptrdiff_t funclen;							// Length of the function name
		struct node *children;						// Beginning memory address of the children nodes
	} vars[VAR_CAPACITY];					// Array to store the variables
} varstore;

/*
static struct {
	size_t size;								 // Current number of functions stored in the function
	
	struct {
		const uint8_t *beg;					   // Beginning memory address of the function name
		ptrdiff_t len;
		struct node *children;
		struct vStore varstore;
	} get[VAR_CAPACITY];
} function;
*/
typedef struct {
	uint8_t type;
	
	union {
		double Number;
		char* String;
		bool Bool;
	} value;
} expr;

struct node;

void visitor(uint8_t, const struct node *);

#endif