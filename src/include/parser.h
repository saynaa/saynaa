#pragma once

#ifndef PARSER_H
#define PARSER_H

#include <stdint.h>
#include <stddef.h>

//capacity of variable & char length
#ifndef VAR_CAPACITY
	#define VAR_CAPACITY 1024
#endif
	
enum {
	NT_Unit,	// Non-terminal symbol for the entire program or unit
	NT_Stmt,	// Non-terminal symbol for a statement
	NT_Assn,	// Non-terminal symbol for an assignment statement
	NT_Ctrl,	// Non-terminal symbol for a control statement (e.g., if, while, do-while)
	NT_Func,	// Non-terminal symbol for an function statement
	NT_Fdec,	// Non-terminal symbol for an function declaration statement
	NT_Fcal,	// Non-terminal symbol for an function call statement
	NT_Cond,	// Non-terminal symbol for a conditional expression (e.g., if condition)
	NT_Elif,	// Non-terminal symbol for an else-if clause
	NT_Else,	// Non-terminal symbol for an else clause
	NT_Forl,	// Non-terminal symbol for a for loop
	NT_Dowh,	// Non-terminal symbol for a do-while loop
	NT_Whil,	// Non-terminal symbol for a while loop
	NT_Atom,	// Non-terminal symbol for an atomic expression
	NT_Expr,	// Non-terminal symbol for an expression
	NT_Cexp,	// Non-terminal symbol for an comma + expression
	NT_Pexp,	// Non-terminal symbol for a Parenthesize expression
	NT_Bexp,	// Non-terminal symbol for a binary expression
	NT_Uexp,	// Non-terminal symbol for a unary expression
	NT_Texp,	// Non-terminal symbol for a term expression
	NT_Aexp,	// Non-terminal symbol for an arithmetic expression
	NT_COUNT,	// Total count of non-terminal symbols
};

static const char *const nts[NT_COUNT] = {
	"Unit", 
	"Stmt", 
	"Assn", 
	"Ctrl", 
	"Func",
	"Fdec",
	"Fcal",
	"Cond", 
	"Elif", 
	"Else", 
	"Forl", 
	"Dowh", 
	"Whil", 
	"Atom", 
	"Expr", 
	"Cexp", 
	"Pexp", 
	"Bexp", 
	"Uexp", 
	"Texp", 
	"Aexp", 
};

enum {		
	TYPE_NUM,			// type number
	TYPE_STR,			// type string
	TYPE_BOL,			// type bool
	TYPE_NUL,			// type null
};

struct token;
struct node {
	/* use "token" if nchildren == 0, "nt" and "children" otherwise */
	uint32_t nchildren;
	union {
		const struct token *token;
		struct {
			uint8_t nt;
			struct node **children;
		};
	};
};

struct node parser(uint8_t, const struct token *, size_t);

enum {
	PARSER_OK,	 // Parsing was successful
	PARSER_REJECT, // Parsing was rejected or encountered an error
	PARSER_NOMEM,  // Parsing failed due to memory allocation failure
};

#define parser_error(root) ({ \
	struct node root_once = (root); \
	root_once.nchildren ? PARSER_OK : root_once.token->tk; \
})

void destroy_tree(struct node);

#endif