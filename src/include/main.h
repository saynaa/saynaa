#pragma once

#ifndef MAIN_H
#define MAIN_H

#define LANGUAGE							"saynaa"
#define VERSION_MAJOR					   "1"
#define VERSION_MINOR					   "2"
#define VERSION_RELEASE					   "1"

#define VERSION_NUM						   121
#define VERSION_RELEASE_NUM				 (VERSION_NUM * 100 + 6)

#define VERSION								LANGUAGE " " VERSION_MAJOR "." VERSION_MINOR
#define RELEASE								VERSION "." VERSION_RELEASE
#define COPYRIGHT							RELEASE "  Copyright (C) 2022-2023 mahdiware.me"
#define AUTHORS								"Mohamed Abdifatah (Mahdiware)"


typedef enum  {
	NONE,
	REPL,
	MSEC
} action_type;

void repl_mode(int , char **);
#endif