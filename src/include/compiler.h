#pragma once

#ifndef COMPILER_H
#define COMPILER_H

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdbool.h>

typedef enum {
	Exit_Success,
	Exit_Lexer,
	Exit_Parser,
	Exit_Visitor,
} Status;

typedef struct {
	Status exit_status;
	int size;
	uint8_t *code;
	uint8_t debug;
	double *msec;
} compiler_info;

void compiler_run(compiler_info *);

#endif