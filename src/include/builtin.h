#pragma once

#ifndef BUILTIN_H
#define BUILTIN_H

#include <stdint.h>
#include <stddef.h>

#ifndef FractionDigits
	#define FractionDigits			12
#endif
#ifndef PrintError
	#define PrintError(...)			fprintf(stderr, __VA_ARGS__);
#endif
#ifndef PrintOut
	#define PrintOut(...) 			printf(__VA_ARGS__);
#endif

#endif