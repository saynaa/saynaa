#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "include/builtin.h"
#include "include/compiler.h"
#include "include/lexer.h"
#include "include/parser.h"
#include "include/visitor.h"
#include "include/utils.h"


void compiler_run(compiler_info *info)
{
	struct token *tokens;
	size_t ntokens;
	
	nanotime_t lexerstart = nanotime();
	const int lex_error = lexer(info->code, info->size, &tokens, &ntokens);
	nanotime_t lexerend = nanotime();
	info->msec[1] = millitime(lexerstart, lexerend);
	
	if ((lex_error == LEX_UNKNOWN_TOKEN)) {
		for (size_t i = 0; i < ntokens; ++i) {
			const struct token token = tokens[i];
			const int len = (token.end - token.beg) ?: 1;
			
			char *character = (char *)malloc((len+1) * sizeof(char *));
			sprintf(character, "%.*s", len, token.beg);
			
			//Check if exits last newline and updated len
			int strlen = character[len-1] == '\n' ? len-1 : len;
			
			if (i == ntokens - 1 && lex_error == LEX_UNKNOWN_TOKEN) {
				PrintError("SyntaxError: line %d invalid syntax '%.*s'", token.line, strlen, token.beg);
			}
			free(character);
		}
	} else if (lex_error == LEX_NOMEM) {
		puts(RED("The lexer could not allocate memory."));
	}
	if (lex_error){
		info->exit_status = Exit_Lexer;
		return;
	}
	
	nanotime_t parserstart = nanotime();
	struct node root = parser(info->debug, tokens, ntokens);
	nanotime_t parserend = nanotime();
	info->msec[2] = millitime(parserstart, parserend);
	
	if (parser_error(root)){
		info->exit_status = Exit_Parser;
		return;
	}
	
	nanotime_t visitorstart = nanotime();
	visitor(info->debug, &root);
	nanotime_t visitorend = nanotime();
	info->msec[3] = millitime(visitorstart, visitorend);
	
	destroy_tree(root);
	free(tokens);
	info->exit_status = Exit_Success;
	return;
}
