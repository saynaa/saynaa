#include "include/lexer.h"
#include "include/utils.h"
#include "include/parser.h"
#include "include/visitor.h"
#include "include/builtin.h"
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

static void visit_stmt(const struct node *const);	 // Function to execute a statement node
static void visit_assn(const struct node *const);	 // Function to execute an assignment node
static void visit_fdec(const struct node *const);
static void visit_fcal(const struct node *const);
static void visit_exit(const struct node *const);
static void visit_sysm(const struct node *const);
static void visit_prnt(const struct node *const);
static void visit_ctrl(const struct node *const);	 // Function to execute a control node

const struct node *getVarName(const struct node *);
static expr getExpr(const struct node *);

void visitor(uint8_t usage_debug, const struct node *const unit)
{
	#if defined(DEBUG)
		if (usage_debug)
			puts(WHITE("\n*** Visitor ***"));
	#endif
	
	// Execute each statement in the program
	for (size_t stmt_idx = 1; stmt_idx < unit->nchildren - 1; ++stmt_idx) {
		visit_stmt(unit->children[stmt_idx]);
	}
	
	// Free the memory associated with variable values
	for (size_t var_idx = 0; var_idx < varstore.size; ++var_idx) {
		free(varstore.vars[var_idx].stringValue);
		free(varstore.vars[var_idx].numberValue);
		free(varstore.vars[var_idx].boolValue);
	}
	
	// Reset the size of the variable store
	varstore.size = 0;
}

static void visit_stmt(const struct node *const stmt)
{
	switch (stmt->children[0]->nt) {
		case NT_Assn: {							  // If the statement is an assignment statement
			visit_assn(stmt->children[0]);			   // Execute the visit_assn function for the assignment
		} break;
		case NT_Ctrl: {							  // If the statement is a control statement
			visit_ctrl(stmt->children[0]);			   // Execute the visit_ctrl function for the control statement
		} break;
		default: {
			abort();								   // If the statement is of an unknown type, abort the program
		} break;
	}
}

static void visit_assn(const struct node *const assn)
{
	
	// Check if the left-hand side of the assignment is an array expression
	const int lhs_is_aexp = assn->children[0]->nchildren;
	
	// Evaluate the array index if present, or set it to 0
	const int array_idx = lhs_is_aexp ? stringToInt(numberConvertor(getExpr(assn->children[0]->children[2]).value.Number)) : 0;
	
	// Get the beginning position and length of the variable name or array name
	const uint8_t *const beg = lhs_is_aexp ?
		assn->children[0]->children[0]->token->beg :
		assn->children[0]->token->beg;
	
	const ptrdiff_t len = lhs_is_aexp ?
		assn->children[0]->children[0]->token->end - beg :
		assn->children[0]->token->end - beg;
	
	size_t var_idx;
	
	// Search for the variable in the variable store
	for (var_idx = 0; var_idx < varstore.size; ++var_idx) {
		// Check if the variable name matches and has the same length
		if (varstore.vars[var_idx].len == len && !memcmp(varstore.vars[var_idx].beg, beg, len)) {
			const size_t array_size = varstore.vars[var_idx].array_size;
			// Check if the variable is a non-array variable
			if (!array_size) {
				PrintError("warn: a previous reallocation has failed, "
					"assignment has no effect\n");

				return;
			}
			expr evalExpr = getExpr(assn->children[2]);
			
			// Check if the array index is within the bounds
			if (array_idx >= 0 && array_idx < array_size) {
				// Evaluate the expression on the right-hand side and assign the value
				if(evalExpr.type == TYPE_STR){
					varstore.vars[var_idx].stringValue[array_idx] = evalExpr.value.String;
					varstore.vars[var_idx].type = TYPE_STR;
				}else if(evalExpr.type == TYPE_NUM){
					varstore.vars[var_idx].numberValue[array_idx] = evalExpr.value.Number;
					varstore.vars[var_idx].type = TYPE_NUM;
				}else if(evalExpr.type == TYPE_BOL){
					varstore.vars[var_idx].boolValue[array_idx] = evalExpr.value.Bool;
					varstore.vars[var_idx].type = TYPE_BOL;
				}else if(evalExpr.type == TYPE_NUL){
					varstore.vars[var_idx].type = TYPE_NUL;
				}
				
				return;
			}
			// Check if the array index is larger than the current array size
			else if (array_idx >= 0) {
				// Calculate the new size of the array by doubling it
				const size_t new_size = (array_idx + 1) * 2;

				// Reallocate memory for the array and store the new size
				
				uint8_t **const tmp_str = realloc(
					varstore.vars[var_idx].stringValue, new_size * sizeof(uint8_t *));
				
				double *const tmp_numb = realloc(
					varstore.vars[var_idx].numberValue, new_size * sizeof(double));
				
				bool *const tmp_bol = realloc(
					varstore.vars[var_idx].boolValue, new_size * sizeof(bool));
				
				if (!tmp_str) {
					free(varstore.vars[var_idx].stringValue);
					varstore.vars[var_idx].array_size = 0;
					varstore.vars[var_idx].stringValue = NULL;
					perror("realloc");
					return;
				}
				
				if (!tmp_bol) {
					free(varstore.vars[var_idx].boolValue);
					varstore.vars[var_idx].array_size = 0;
					varstore.vars[var_idx].boolValue = NULL;
					perror("realloc");
					return;
				}
				
				if (!tmp_numb) {
					free(varstore.vars[var_idx].numberValue);
					varstore.vars[var_idx].array_size = 0;
					varstore.vars[var_idx].numberValue = NULL;
					perror("realloc");
					return;
				}
				
				varstore.vars[var_idx].stringValue = tmp_str;
				varstore.vars[var_idx].numberValue = tmp_numb;
				varstore.vars[var_idx].boolValue = tmp_bol;
				varstore.vars[var_idx].array_size = new_size;
				
				expr evalExpr = getExpr(assn->children[2]);
				
				// Evaluate the expression on the right-hand side and assign the value
				if(evalExpr.type == TYPE_STR){
					varstore.vars[var_idx].stringValue[array_idx] = evalExpr.value.String;
					varstore.vars[var_idx].type = TYPE_STR;
				}else if(evalExpr.type == TYPE_NUM){
					varstore.vars[var_idx].numberValue[array_idx] = evalExpr.value.Number;
					varstore.vars[var_idx].type = TYPE_NUM;
				}else if(evalExpr.type == TYPE_BOL){
					varstore.vars[var_idx].boolValue[array_idx] = evalExpr.value.Bool;
					varstore.vars[var_idx].type = TYPE_BOL;
				}else if(evalExpr.type == TYPE_NUL){
					varstore.vars[var_idx].type = TYPE_NUL;
				}
				
				return;
			} else {
				PrintError("warn: negative array offset\n");
				return;
			}
		}
	}
	
	// If the variable was not found in the variable store
	if (var_idx < VAR_CAPACITY) {
		// Check if the array index is negative
		if (array_idx < 0) {
			PrintError("warn: negative array offset\n");
			return;
		}

		// Allocate memory for the variable value array
		varstore.vars[var_idx].beg = beg;
		varstore.vars[var_idx].len = len;
		varstore.vars[var_idx].stringValue = malloc((array_idx + 1) * sizeof(uint8_t *));
		varstore.vars[var_idx].numberValue = malloc((array_idx + 1) * sizeof(double));
		varstore.vars[var_idx].boolValue = malloc((array_idx + 1) * sizeof(bool));
		varstore.vars[var_idx].array_size = 0;
		
		if (!varstore.vars[var_idx].stringValue || !varstore.vars[var_idx].numberValue || !varstore.vars[var_idx].boolValue) {
			perror("malloc");
			return;
		}
		varstore.vars[var_idx].array_size = array_idx + 1;	
		expr evalExpr = getExpr(assn->children[2]);

		if(evalExpr.type == TYPE_STR){
			varstore.vars[var_idx].stringValue[array_idx] = evalExpr.value.String;
			varstore.vars[var_idx].type = TYPE_STR;
		}else if(evalExpr.type == TYPE_NUM){
			varstore.vars[var_idx].numberValue[array_idx] = evalExpr.value.Number;
			varstore.vars[var_idx].type = TYPE_NUM;
		}else if(evalExpr.type == TYPE_BOL){
			varstore.vars[var_idx].boolValue[array_idx] = evalExpr.value.Bool;
			varstore.vars[var_idx].type = TYPE_BOL;
		}else if(evalExpr.type == TYPE_NUL){
			varstore.vars[var_idx].type = TYPE_NUL;
		}
		
		varstore.size++;
	} else {
		PrintError("warn: varstore exhausted, assignment has no effect\n");
	}
}

static void visit_fdec(const struct node *const func)
{
	int pramloop = 0;
	uint8_t pramtk1, pramtk2;
		
	while(1){
		pramloop++;
		pramtk1 = func->children[2+pramloop]->token->tk;
		pramtk2 = func->children[3+pramloop]->token->tk;
		
		if(pramtk1 == TK_RPAR && pramtk2 == TK_LBRC){
			pramloop--;
			break;
		}
		
		if(getVarName(func->children[2+pramloop]) == NULL){
			PrintError("ParamError: <name> or '...' not other datatype\n");
			exit(1);
		}
	}
	
	const uint8_t *beg = func->children[1]->token->beg;
	const ptrdiff_t len = func->children[1]->token->end - func->children[1]->token->beg;
	
	struct node *stmt = func->children[5 + (pramloop)];
	
	size_t var_idx;
	for (var_idx = 0; var_idx < varstore.size; ++var_idx) {
		//soon
	}
	if(var_idx < VAR_CAPACITY){
		varstore.vars[var_idx].funcbeg = beg;
		varstore.vars[var_idx].funclen = len;
		
		
		for(int i = 1; i <= pramloop; i++){
			const struct node *pnode = getVarName(func->children[2+i]);
			if(pnode != NULL){
				const uint8_t *const sbeg = pnode->token->beg;
				const ptrdiff_t slen = pnode->token->end - sbeg;

				varstore.vars[var_idx+i].beg = sbeg;
				varstore.vars[var_idx+i].len = slen;
				varstore.vars[var_idx+i].stringValue = malloc((1) * sizeof(uint8_t *));
				varstore.vars[var_idx+i].numberValue = malloc((1) * sizeof(double));
				varstore.vars[var_idx+i].boolValue = malloc((1) * sizeof(bool));
				varstore.vars[var_idx+i].array_size = 0;

				if (!varstore.vars[var_idx+i].stringValue || !varstore.vars[var_idx+i].numberValue || !varstore.vars[var_idx+i].boolValue) {
					perror("malloc");
					return;
				}

				varstore.vars[var_idx+i].array_size = 1;
				varstore.vars[var_idx+i].type = TYPE_NUL;
				varstore.size++;
			}
		}
		
		varstore.vars[var_idx].children = stmt;
		varstore.size++;
	}
}

static void visit_fcal(const struct node *const func)
{
	const uint8_t *beg = func->children[0]->token->beg;
	const ptrdiff_t len = func->children[0]->token->end - func->children[0]->token->beg;
	
	char *FuncName = (char *)malloc((len+1) * sizeof(char *));
	sprintf(FuncName, "%.*s", len, beg);
	
	if(contains(FuncName, "prnt") || contains(FuncName, "print")){
		visit_prnt(func);
		return;
	}else if(contains(FuncName, "exit")){
		visit_exit(func);
		return;
	}else if(contains(FuncName, "exec")){
		visit_sysm(func);
		return;
	}
	
	for (size_t idx = 0; idx < varstore.size; ++idx) {
		if (varstore.vars[idx].funclen == len && !memcmp(varstore.vars[idx].funcbeg, beg, len)) {
			if(func->nchildren >= 4){
				int totalparam = func->nchildren - 4;
				if(func->children[totalparam+2]->token->tk == TK_RPAR && func->children[totalparam+3]->token->tk == TK_SCOL){
					for(int i = 1; i <= totalparam; i++){
						expr evalExpr = getExpr(func->children[i+1]);
												
						if(evalExpr.type == TYPE_STR){
							varstore.vars[idx+i].type = TYPE_STR;
							varstore.vars[idx+i].stringValue[0] = evalExpr.value.String;
						}else if(evalExpr.type == TYPE_NUM){
							varstore.vars[idx+i].type = TYPE_NUM;
							varstore.vars[idx+i].numberValue[0] = evalExpr.value.Number;
						}else if(evalExpr.type == TYPE_BOL){
							varstore.vars[idx+i].type = TYPE_BOL;
							varstore.vars[idx+i].boolValue[0] = evalExpr.value.Bool;
						}else if(evalExpr.type == TYPE_NUL){
							varstore.vars[idx+i].type = TYPE_NUL;
						}
					}
				}
			}
			while (varstore.vars[idx].children->nchildren) {
				visit_stmt(varstore.vars[idx].children++);
			}
			return;
		}
	}
	PrintError("NameError: name '%.*s' is not defined\n", len, beg);
}

static void visit_sysm(const struct node *const sysm)
{
	if(sysm->nchildren >= 3){
		int totalparam = sysm->nchildren - 4;
		for(int i = 0; i < totalparam; i++){
			if(i == totalparam-1){
				expr evalExpr = getExpr(sysm->children[i+2]);
				if(evalExpr.type == TYPE_STR){
					system(evalExpr.value.String);
				}
			}
		}
	}
}

static void visit_exit(const struct node *const pexit)
{
	if(pexit->nchildren >= 3){
		int totalparam = pexit->nchildren - 4;
		for(int i = 0; i < totalparam; i++){
			if(i != totalparam-1){
				exit(1);
			}else{
				expr evalExpr = getExpr(pexit->children[i+2]);
				if(evalExpr.type == TYPE_NUM){
					exit(evalExpr.value.Number);
				}
			}
		}
		exit(0);
	}
	PrintError("PramError: unsupported other datatype only integer\n");
	exit(1);
}

static void visit_prnt(const struct node *const func)
{
	if(func->nchildren >= 4){
		int totalparam = func->nchildren - 4;
		for(int i = 0; i < totalparam; i++){
			expr evalExpr = getExpr(func->children[i+2]);
			if(evalExpr.type == TYPE_STR){
				printf("%s", evalExpr.value.String);
			}else if(evalExpr.type == TYPE_NUM){
				printf("%s", numberConvertor(evalExpr.value.Number));
			}else if(evalExpr.type == TYPE_BOL){
				printf("%s", evalExpr.value.Bool ? "(true)" : "(false)");
			}else if(evalExpr.type == TYPE_NUL){
				printf("(NULL)");
			}
		}
		return;
	}else if(func->nchildren == 3 && func->children[1]->token->tk == TK_STRL){
		const uint8_t *const beg = func->children[1]->token->beg + 1;
		const ptrdiff_t len = (func->children[1]->token->end - 1) - beg;
		
		char *getSTRL = (char *)malloc((len) * sizeof(char *));
		sprintf(getSTRL, "%.*s", len, beg);
		ScapeSequence(&getSTRL);
		
		printf("%s", getSTRL);
		free(getSTRL);
	}
}

static void visit_ctrl(const struct node *const ctrl)
{
	switch (ctrl->children[0]->nt) {
		case NT_Func: {
			switch(ctrl->children[0]->children[0]->nt){
				case NT_Fcal: {
					visit_fcal(ctrl->children[0]->children[0]);
				} break;
				case NT_Fdec: {
					visit_fdec(ctrl->children[0]->children[0]);
				} break;
			};
		} break;
		case NT_Cond: {
			// Conditional statement
			const struct node *const cond = ctrl->children[0];
	
			// Evaluate the condition expression
			if (getExpr(cond->children[1]).value.Bool) {
				// Execute the statements in the true branch
				const struct node *stmt = cond->children[3];
	
				while (stmt->nchildren) {
					visit_stmt(stmt++);
				}
			} else if (ctrl->nchildren >= 2) {
				// Check for "elif" and "else" branches
				size_t child_idx = 1;
	
				do {
					if (ctrl->children[child_idx]->nt == NT_Elif) {
						// Evaluate the condition expression of the "elif" branch
						const struct node *const elif = ctrl->children[child_idx];
	
						if (getExpr(elif->children[1]).value.Bool) {
							// Execute the statements in the true branch of "elif"
							const struct node *stmt = elif->children[3];
	
							while (stmt->nchildren) {
								visit_stmt(stmt++);
							}
	
							break;
						}
					} else {
						// Execute the statements in the "else" branch
						const struct node *const els = ctrl->children[child_idx];
						const struct node *stmt = els->children[2];
	
						while (stmt->nchildren) {
							visit_stmt(stmt++);
						}
					}
				} while (++child_idx < ctrl->nchildren);
			}
		} break;
	
		case NT_Dowh: {
			// Do-while loop
			const struct node *const dowh = ctrl->children[0];
			const struct node *const expr = dowh->children[dowh->nchildren - 2];
	
			do {
				// Execute the statements in the loop body
				const struct node *stmt = dowh->children[2];
	
				while (stmt->nchildren) {
					visit_stmt(stmt++);
				}
			} while (getExpr(expr).value.Bool);
		} break;
	
		case NT_Whil: {
			// While loop
			const struct node *const whil = ctrl->children[0];
	
			while (getExpr(whil->children[1]).value.Bool) {
				// Execute the statements in the loop body
				const struct node *stmt = whil->children[3];
	
				while (stmt->nchildren) {
					visit_stmt(stmt++);
				}
			}
		} break;
	
		default: {
			abort();
		} break;
	}
}






/*==========================================================================================*/
/*======================================== Expression ========================================*/
/*==========================================================================================*/

const struct node *getVarName(const struct node * varname_t)
{
	if(varname_t->children[0]->nt == NT_Atom){
		if(varname_t->children[0]->children[0]->token->tk == TK_NAME){
			return varname_t->children[0]->children[0];
		}
	}else if(varname_t->children[0]->nt == NT_Cexp){
		if(varname_t->children[0]->children[0]->children[0]->children[0]->token->tk == TK_NAME){
			return varname_t->children[0]->children[0]->children[0]->children[0];
		}
	}
	return NULL;
}

void eval_expr(const struct node *const, uint8_t *, double *, bool *, char **);

static expr getExpr(const struct node * expr_t)
{
	expr expr_v;
	double g_number = 0.0;
	bool g_bool = false;
	char *g_string = NULL;
	uint8_t type;
	
	eval_expr(expr_t, &type, &g_number, &g_bool, &g_string);
	
	expr_v.type = type;
	if(type == TYPE_NUM){
		expr_v.value.Number = g_number;
	}else if(type == TYPE_STR){
		expr_v.value.String = g_string;
	}else if(type == TYPE_BOL){
		expr_v.value.Bool = g_bool;
	}
	return expr_v;
}

void eval_atom(const struct node *const atom, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	switch (atom->children[0]->token->tk) {
		case TK_NAME: {
			// Variable name
			const uint8_t *const beg = atom->children[0]->token->beg;
			const ptrdiff_t len = atom->children[0]->token->end - beg;
			
			// Search for the variable in the varstore
			for (size_t idx = 0; idx < varstore.size; ++idx) {
				if (varstore.vars[idx].len == len && !memcmp(varstore.vars[idx].beg, beg, len)) {
					// Check if the variable is an array
					if (varstore.vars[idx].array_size) {
						if(varstore.vars[idx].type == TYPE_STR){
							*stringPtr = (char *)varstore.vars[idx].stringValue[0];
							*type = TYPE_STR;
						}else if(varstore.vars[idx].type == TYPE_NUM){
							*numberPtr = varstore.vars[idx].numberValue[0];
							*type = TYPE_NUM;
						}else if(varstore.vars[idx].type == TYPE_BOL){
							*boolPtr = varstore.vars[idx].boolValue[0];
							*type = TYPE_BOL;
						}else if(varstore.vars[idx].type == TYPE_NUL){
							*stringPtr = NULL;
							*type = TYPE_NUL;
						}
						
						return;
					} else {
						*stringPtr = NULL;
						*type = TYPE_NUL;
						return;
					}
				}
			}
			
			PrintError("warn: access to undefined variable\n");
			*stringPtr = NULL;
			*type = TYPE_NUL;
			return;
		} break;
		case TK_TRUE: {
			*boolPtr = true;
			*type = TYPE_BOL;
			return;
		} break;
		case TK_FALSE: {
			*boolPtr = false;
			*type = TYPE_BOL;
			return;
		} break;
		case TK_NULL: {
			*stringPtr = NULL;
			*type = TYPE_NUL;
			return;
		} break;
		case TK_STRL: {
			const uint8_t *const beg = atom->children[0]->token->beg + 1;
			const ptrdiff_t len = (atom->children[0]->token->end - 1) - beg;
			
			char *tempstring = (char *)malloc((len+1) * sizeof(char *));
			sprintf(tempstring, "%.*s", len, beg);
			
			ScapeSequence(&tempstring);
			
			*stringPtr = tempstring;
			*type = TYPE_STR;
			//free(tempstring);
			return;
		} break;
		case TK_NMBR: {
			// Numeric value
			const uint8_t *const beg = atom->children[0]->token->beg;
			const uint8_t *const end = atom->children[0]->token->end;

			char *StrNum = (char *)malloc((end - beg + 1) * sizeof(char *));
			sprintf(StrNum, "%.*s", end - beg, beg);

			*numberPtr = stringToNumber(StrNum);
			*type = TYPE_NUM;
			free(StrNum);
			return;
		} break;

		default: {
			abort();
		} break;
	}
}

void eval_cexp(const struct node *const cexp, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	eval_expr(cexp->children[0], type, numberPtr, boolPtr, stringPtr);
}

void eval_pexp(const struct node *const pexp, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	// Parenthesized expression, evaluate the expression inside
	eval_expr(pexp->children[1], type, numberPtr, boolPtr, stringPtr);
}

void eval_bexp(const struct node *const bexp, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	switch (bexp->children[1]->token->tk) {
		case TK_PLUS: {
			// Addition
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*numberPtr = evalExpr1.value.Number + evalExpr2.value.Number;
					*type = TYPE_NUM;
				}
			}
		} break;

		case TK_MINS: {
			// Subtraction
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*numberPtr = evalExpr1.value.Number - evalExpr2.value.Number;
					*type = TYPE_NUM;
				}
			}
		} break;

		case TK_MULT: {
			// Multiplication
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*numberPtr = evalExpr1.value.Number * evalExpr2.value.Number;
					*type = TYPE_NUM;
				}
			}
		} break;

		case TK_DIVI: {
			// Division
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					if(evalExpr2.value.Number){
						*numberPtr = evalExpr1.value.Number / evalExpr2.value.Number;
						*type = TYPE_NUM;
					}else{
						PrintError("warn: prevented attempt to divide by zero\n");
						*numberPtr = 0.0;
						*type = TYPE_NUM;
					}
				}
			}
		} break;

		case TK_MODU: {
			// Modulo
			
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*numberPtr = fmod(evalExpr1.value.Number, evalExpr2.value.Number);
					*type = TYPE_NUM;
				}
			}
		} break;

		case TK_EQUL: {
			// Equality
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number == evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		case TK_NEQL: {
			// Inequality
			
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number != evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		case TK_LTHN: {
			// Less than
			
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number < evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		case TK_GTHN: {
			// Greater than
			
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number > evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		case TK_LTEQ: {
			// Less than or equal to
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number <= evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		case TK_GTEQ: {
			// Greater than or equal to
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number >= evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		case TK_CONJ: {
			// Logical conjunction (AND)
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number && evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		case TK_DISJ: {
			// Logical disjunction (OR)
			expr evalExpr1 = getExpr(bexp->children[0]);
			expr evalExpr2 = getExpr(bexp->children[2]);
			
			if(evalExpr1.type == TYPE_NUM){
				if(evalExpr2.type == TYPE_NUM){
					*boolPtr = evalExpr1.value.Number || evalExpr2.value.Number;
					*type = TYPE_BOL;
				}
			}
		} break;

		default: {
			abort();
		} break;
	}
}

void eval_uexp(const struct node *const uexp, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	// Evaluate unary expressions
	switch (uexp->children[0]->token->tk) {
		case TK_PLUS: {
			// Unary plus
			eval_expr(uexp->children[1], type, numberPtr, boolPtr, stringPtr);
		} break;

		case TK_MINS: {
			// Unary minus
			*numberPtr = -(getExpr(uexp->children[1]).value.Number);
		} break;

		case TK_PLPL: {
			// Unary minus
			*numberPtr = (getExpr(uexp->children[1]).value.Number)+1;
		} break;

		case TK_MIMI: {
			// Unary minus
			*numberPtr = (getExpr(uexp->children[1]).value.Number)+1;
		} break;

		case TK_NEGA: {
			// Logical negation
			expr evalExpr = getExpr(uexp->children[1]);
			if(evalExpr.type == TYPE_NUM){
				*boolPtr = !evalExpr.value.Number;
				*type = TYPE_BOL;
			} else if(evalExpr.type == TYPE_BOL){
				*boolPtr = !evalExpr.value.Bool;
				*type = TYPE_BOL;
			}
		} break;

		default: {
			prnt(10, "__");
			abort();
		} break;
	}
}

void eval_texp(const struct node *const texp, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	// Evaluate ternary expressions
	bool condition = getExpr(texp->children[0]).value.Bool;
	if (condition) {
		eval_expr(texp->children[2], type, numberPtr, boolPtr, stringPtr);
	} else {
		eval_expr(texp->children[4], type, numberPtr, boolPtr, stringPtr);
	}
}

void eval_aexp(const struct node *const aexp, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	// Evaluate array expressions
	const uint8_t *const beg = aexp->children[0]->token->beg;
	const ptrdiff_t len = aexp->children[0]->token->end - beg;
	
	const int array_idx = stringToInt(numberConvertor(getExpr(aexp->children[2]).value.Number));
	
	// Check if the array index is negative
	if (array_idx < 0) {
		PrintError("warn: negative array offset\n");
		*stringPtr = NULL;
		*type = TYPE_NUL;
		return;
	}

	// Search for the array in the varstore
	for (size_t idx = 0; idx < varstore.size; ++idx) {
		if (varstore.vars[idx].len == len && !memcmp(varstore.vars[idx].beg, beg, len)) {
			// Check if the array index is within the bounds
			if (array_idx < varstore.vars[idx].array_size) {
				if(varstore.vars[idx].type == TYPE_STR){
					*stringPtr = (char *)varstore.vars[idx].stringValue[array_idx];
					*type = TYPE_STR;
				}else if(varstore.vars[idx].type == TYPE_NUM){
					*numberPtr = varstore.vars[idx].numberValue[array_idx];
					*type = TYPE_NUM;
				}else if(varstore.vars[idx].type == TYPE_BOL){
					*boolPtr = varstore.vars[idx].boolValue[array_idx];
					*type = TYPE_BOL;
				}
				return;
			} else {
				PrintError("warn: out of bounds array access\n");
				*stringPtr = NULL;
				*type = TYPE_NUL;
				return;
			}
		}
	}
	
	PrintError("warn: access to undefined array\n");
	*stringPtr = NULL;
	*type = TYPE_NUL;
}


void eval_expr(const struct node *const expr, uint8_t *type, double *numberPtr, bool *boolPtr, char **stringPtr)
{
	switch (expr->children[0]->nt) {
		case NT_Atom: {
			eval_atom(expr->children[0], type, numberPtr, boolPtr, stringPtr);
		} break;

		case NT_Cexp: {
			eval_cexp(expr->children[0], type, numberPtr, boolPtr, stringPtr);
		} break;

		case NT_Pexp: {
			eval_pexp(expr->children[0], type, numberPtr, boolPtr, stringPtr);
		} break;

		case NT_Bexp: {
			eval_bexp(expr->children[0], type, numberPtr, boolPtr, stringPtr);
		} break;

		case NT_Uexp: {
			eval_uexp(expr->children[0], type, numberPtr, boolPtr, stringPtr);
		} break;

		case NT_Texp: {
			eval_texp(expr->children[0], type, numberPtr, boolPtr, stringPtr);
		} break;

		case NT_Aexp: {
			eval_aexp(expr->children[0], type, numberPtr, boolPtr, stringPtr);
		} break;

		default: {
			abort();
		} break;
	}
}