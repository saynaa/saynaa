#-Wall -Werror
SYSCFLAGS=
SYSLDFLAGS=
CFLAGS = -std=gnu11 -fgnu89-inline -fPIC
NAME = saynaa
CC ?= gcc

SRCDIR := ./src
OBJDIR := ./obj
SRCS := $(addprefix $(SRCDIR)/, $(wildcard $(SRCDIR)/*.c))
OBJS := $(addprefix $(OBJDIR)/, $(notdir $(SRCS:.c=.o)))

ifeq ($(OS),Windows_NT)
	# Windows
	LIBTARGET = $(NAME).dll
	LDFLAGS = -lm -lShlwapi
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Darwin)
		# MacOS
		LIBTARGET = lib$(NAME).dylib
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),OpenBSD)
		# OpenBSD
		LIBTARGET = lib$(NAME).so
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),FreeBSD)
		# FreeBSD
		LIBTARGET = lib$(NAME).so
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),NetBSD)
		# NetBSD
		LIBTARGET = lib$(NAME).so
		LDFLAGS = -lm
	else ifeq ($(UNAME_S),DragonFly)
		# DragonFly
		LIBTARGET = lib$(NAME).so
		LDFLAGS = -lm
	else
		# Linux
		LIBTARGET = lib$(NAME).so
		LDFLAGS = -lm -lrt
	endif
endif

all: $(NAME)

readline:
	@$(MAKE) $(ALL) SYSLDFLAGS="-lreadline" SYSCFLAGS="-DREADLINE"

debug:
	$(MAKE) $(ALL) SYSCFLAGS="-DDEBUG"	

readline-and-debug:
	@$(MAKE) $(ALL) SYSLDFLAGS="-lreadline" SYSCFLAGS="-DREADLINE -DDEBUG"

$(OBJS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	@mkdir -p $(OBJDIR)
	$(CC) $(CFLAGS) $(SYSCFLAGS) -c $< -o $@

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $^ -lm $(SYSLDFLAGS)

lib: $(NAME)
	$(CC) -shared -o $(LIBTARGET) $(OBJS) $(LDFLAGS)

c: clean
i: install

clean:
	rm -rf $(OBJDIR)
	rm -f $(NAME)

install:
	@cp -r $(NAME) /usr/local/bin/
	@chmod +x /usr/local/bin/$(NAME)
	@printf "\033[38;5;52m\033[43m\t    installed!    \t\033[0m\n";
