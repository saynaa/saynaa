<p align="center"> <img src="https://raw.githubusercontent.com/mahdiware/saynaa/master/docs/assets/images/logo.png" alt="Interpreter-Language" title=Logo" align="center"> </p>

## The Language
is a powerful, lightweight, embeddable programming language written in C without any external dependencies (except for stdlib).

## Getting started

### Install
To install, simply execute the commands given below.

```bash
	git clone https://github.com/mahdiware/saynaa.git
	cd saynaa
	make
```

### Command line
To view all possible flags you can run the command below:
```bash
	./saynaa --help
```

To directly execute a file
```bash
	./saynaa myfile.sa
```

## Donate
 [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.me/mahdiware)
 
## Version
 v1.0:
- Added the print() function.
- Created the example/ directory for testing.
- Applied the MakeFile.
- Fixed bugs in the print() function and added support for printing strings only, integers only, and both.
- Added a variable to store a string.
- Created a new function called 'input' that inserts and updates variable values.

v1.1:
- Updated the input() function and applied it to integers and string only.
- Added the varstore type to the enum: 'VAR_INT, VAR_STR, VAR_FLT'.
- Expanded support to include floating-point numbers, whereas previously only integers were supported.
- Added the stringToFloat() function in run.c to convert strings to floats and return a float value.

v1.2.0:
- added function
- added float
- updated Expression and can be use `string`, `int`, `float`, `boolean` and `null` while before can only use `int`
- updated print() function and support to print all data-type
- added function param
- added execute millisecond
- updated assignment and can use all data-type
- solved bugs boolean and float

v1.2.1:
- I solved the Binary expression and it works fine now 
- I added function parameter
- I have prepared a utility buildIn function of `print()`, `exit()`, `exec()`
- You can easily use all the `lexer`, `parser` and `visitor` and I called it `compiler.c`
- I have done Unary Expression update on all of them
- I added a function called getVarName() which returns the variable name, instead of using the variable value in expression. 
- I made it fast when the program was running and I removed it and managed it and divided it between what I needed and what was extra, finally it is now fast. 


## example code

for example these code: 
```sa
number = 0;

do {
    print("Hello ", "World!\n");
    
    number = number + 1;
} while (number <= 10);
```

and excused the code
```bash
$ make
$ ./saynaa filename.sa
Hello World!
Hello World!
Hello World!
Hello World!
Hello World!
Hello World!
Hello World!
Hello World!
Hello World!
Hello World!
Hello World!
```

## License
Saynaa is available under the permissive MIT license.
